import React, { useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { NavigationContainer, useNavigation } from '@react-navigation/native';
import MovieSearchScreen from './components/MovieSearchScreen';
import AboutScreen from './components/AboutScreen';
import FavoritesScreen from './components/FavoritesScreen';
import MovieNavigationTab from './components/MovieNavigationTab';
import { Button, StyleSheet } from 'react-native';
import { FavoritesContext, FavoritesContextProvider } from './src/FavoritesContext';
import { MoviesContextProvider } from './src/MoviesContext';
import { SearchContextProvider } from './src/SearchContext';
import SettingsScreen from './components/SettingsScreen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const Stack = createStackNavigator()


export default function App() {
  useEffect(() => {
    console.log('App');
    return () => {

    }
  }, [])

  return (
    <SearchContextProvider>
      <MoviesContextProvider>
        <FavoritesContextProvider>
          <NavigationContainer>
            <Stack.Navigator initialRouteName="g" headerMode='screen' screenOptions={({route, navigation}) =>
              ({
                headerRight: () => (<FontAwesome5.Button name={'cog'} accessibilityLabel='Settings' backgroundColor='#fff' color='#000' size={30} onPress={() => {
                  console.log("navigate");
                  navigation.navigate('Settings')
                }} />),
              })
            }>
              <Stack.Screen name="Movie DB Search" component={MovieNavigationTab} />
            </Stack.Navigator>
          </NavigationContainer>
        </FavoritesContextProvider>
      </MoviesContextProvider>
    </SearchContextProvider>
  )

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        {/* <Stack.Screen name="Home" component={MovieSearchScreen} options={{title: 'Movie DB Search Home'}} />
        <Stack.Screen name="Favorites" component={FavoritesScreen} options={{headerTitle:'Favorites'}} />
        <Stack.Screen name="About" component={AboutScreen} options={{headerTitle: 'About'}} /> */}
        <Stack.Screen name="Home" component={MovieNavigationTab} />
      </Stack.Navigator>
    </NavigationContainer>
  )
  const Tab = createBottomTabNavigator();
  // const Tab = createMaterialTopTabNavigator()

  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="Home" tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
        style: styles.labelStyle,
        labelStyle: { fontSize: 14 },
      }}
        //  tabBarPosition='bottom'
        screenOptions={
          {
            title: 'Movie DB',

          }
        }
      >
        <Tab.Screen name="Home" component={MovieSearchScreen} options={{ title: 'Home' }} />
        <Tab.Screen name="Favorites" component={FavoritesScreen} />
        <Tab.Screen name="About" component={AboutScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  labelStyle: {
    fontSize: 16
  }
})