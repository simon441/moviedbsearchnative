import React, { useContext, useEffect } from 'react'
import { MoviesContext } from './MoviesContext'

const useMovies = () => {
    /** @type {[object[], Function]} */
    const [movies, setMovies] = useContext(MoviesContext)

    useEffect(()=> {
        setMovies([])
    }, [])

    const moviesAdd = (movie) => {
        setMovies(movies => [...movies, movie])
    }

    const moviesRemove = (movie) => {
        const newMovies = movies.filter(mov => mov.imdbID !== movie.imdbID)
        setMovies(newMovies)
    }

    /**
     * Get a movie by its imdb ID
     * @param {number} id
     * @returns {object|null}
     */
    const getById = (id)=> {
        return movies?.filter(mov => mov.imdbID === id)
    }

    const movieExists = (movie) => {
        return movies.find(mov => mov.imdbID === movie.imdbID) !== undefined
    }

    return { movies, moviesAdd, moviesRemove, movieExists, getById }
}

export default useMovies
