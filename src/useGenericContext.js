import React, { Context, useContext } from 'react'
import { View, Text } from 'react-native'

/**
 *
 * @param {Context} genericContext
 */
const useGenericContext = (genericContext) => {
    /** @type {[object[], Function]} */
    const [items, setItems] = useContext(genericContext)

     const add = (item) => {
         setItems(i => [...i, item])
     }

     const remove = (item) => {
         const newItem = items.filter(i => i.imdbID !== item.imdbID)
         setItems(newItem)
     }

     /**
      * Get a favorite by its imdb ID
      * @param {number} id
      * @returns {object|null}
      */
     const getById = (id)=> {
         return items.filter(item => item.imdbID === id)
     }

     const exists = (item) => {
         return items.find(i => i.imdbID === item.imdbID) !== undefined
     }

     return { items, add,remove, exists, getById }
}

export default UseGenericContext
