/**
 * Check if the value is not defined
 * @param {string} value - Data to verify
 */
export const isNotNA = (value) => {
    return value !== 'N/A'
}
