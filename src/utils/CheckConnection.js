import {useState, useEffect} from 'react';
import NetInfo from '@react-native-community/netinfo';

let currentNetwork;
let net;

NetInfo.fetch().then((state) => {
  currentNetwork = state.isConnected;
  net = state
});

const CheckConnection = () => {
  const [netInfo, setNetInfo] = useState(currentNetwork);
  const [network, setNetwork] = useState(net)
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state) => {
      // console.log("Connection type", state.type);
      // console.log("Is connected?", state.isConnected);
      setNetwork(state)
      setNetInfo(state.isConnected);
    });
    return () => unsubscribe();
  }, []);

  return netInfo;
};

export default CheckConnection;