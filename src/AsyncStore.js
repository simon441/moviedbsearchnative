import React from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'

class AsyncStore  {
    constructor(key = '@storage_Key') {
        this.storageKey = key
    }
     storeData = async(value)=> {
        try {
            const jsonValue = JSON.stringify(value)
            await AsyncStorage.setItem(this.storageKey, jsonValue)
        } catch (e) {
            // saving error
            console.error(e);
        }
    }

    setObjectValue = async (value) => {
        try {
          const jsonValue = JSON.stringify(value)
          await AsyncStorage.setItem(this.storageKey, jsonValue)
          console.log('all keysssssssssssssssssssss', await AsyncStorage.getAllKeys(), await AsyncStorage.getItem(this.storageKey));
        } catch(e) {
          // save error
          console.error(err);
        }
      }

     getValue = async() => {
        try {
            const value = await AsyncStorage.getItem(this.storageKey)
            if (value !== null) {
                // value previously stored
                return value
            }
        } catch (e) {
            // error reading value
            console.error(e);
        }
    }


    getObject = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem(this.storageKey)
            console.log('jsonValue', jsonValue);
            return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch (e) {
            // error reading value
            console.error(e);
        }
    }



}

export default AsyncStore
