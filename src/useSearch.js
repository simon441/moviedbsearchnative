import AsyncStorage from "@react-native-async-storage/async-storage"
import { useContext, useEffect } from "react"
import AsyncStore from "./AsyncStore"
import { SearchContext } from "./SearchContext"


export const useSearch = () => {
    const store = new AsyncStore('@searchKey')

    /** @type {[string[], Function]} */
    const [search, setSearch] = useContext(SearchContext)

    useEffect(() => {
        store.getObject()
            .then(value => {
                if (value !== null) {

                    setSearch(value)
                } else {
                    // setSearch([])
                }
                // AsyncStorage.removeItem('@searchKey')
                console.log('effect value', value);
            })
            .catch(err => console.error(err))

    }, [])

    const add = (term) => {
        if (search.includes(term)) {
            return
        }
        setSearch(prev => [...prev, term])
        console.log('add search', search);
        store.setObjectValue(search).then(() => {console.log('added search', search);}
        ).catch(e => console.error(e))
    }

    const remove = (term) => {
        setSearch(prev => prev.filter(t => t !== term))
        store.setObjectValue(search).then(() => {}
        ).catch(e => console.error(e))
    }

    const getAll = () => {
        console.log('getAll', search);
        return search !== undefined ? search : []
    }

    const allStored =()=> {
        return  store.getObject()
        .then(value => value !== undefined ? value : [])
        .catch(e => console.log(e))
    }

    const allStoredValue =()=> {
        return  store.getValue()
        .then(value => value !== undefined ? value : [])
        .catch(e => console.log(e))
    }

    const clear = () => {
        setSearch([])
        store.setObjectValue([]).then(() => {}
        ).catch(e => console.error(e))
    }

    const find = term => {
        return search.find(s => s === term)
    }

    return { search, setSearch, add, remove, getAll, allStored, allStoredValue, clear, find }
}
