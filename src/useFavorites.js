import React, { useContext, useEffect } from 'react'
import { FavoritesContext } from './FavoritesContext'
import AsyncStore from './AsyncStore'
const useFavorites = () => {
    const store = new AsyncStore('@favorites')
    /** @type {[object[], Function]} */
    const [favorites, setFavorites] = useContext(FavoritesContext)

    useEffect(()=> {
        store.getObject().then(value => {
                console.log('value', value);
            if (value !== null) {
                setFavorites(value)
            }
        })
        .catch(e => console.error(e))
    }, [])

    useEffect(() => {
        store.setObjectValue(favorites).then(() => {}
        ).catch(e => console.error(e))
    }, [favorites])

    const favoritesAdd = (favorite) => {
        if (favoriteExists(favorite)) {
            return
        }
        setFavorites(fav => [...fav, favorite])
        store.setObjectValue(favorites).then(() => {}
        ).catch(e => console.error(e))
    }

    const favoritesRemove = (favorite) => {
        const newFavorites = favorites.filter(fav => fav.imdbID !== favorite.imdbID)
        setFavorites(newFavorites)
        store.setObjectValue(favorites).then(() => {}
        ).catch(e => console.error(e))
    }

    /**
     * Get a favorite by its imdb ID
     * @param {number} id
     * @returns {object|null}
     */
    const getById = (id)=> {
        return favorites.filter(fav => fav.imdbID === id)
    }

    const favoriteExists = (favorite) => {
        return favorites.find(fav => fav.imdbID === favorite.imdbID) !== undefined
    }

    const clear = () => {
        setFavorites([])
        store.setObjectValue(favorites)
    }


    const getFavoritesActionText = (movie) => {
        if (favoriteExists(movie) === true) {
            return 'Remove From Favorites'
        }
        return 'Add To Favorites'
    }

    const manageFavorite = (movie) => {
        if (favoriteExists(movie) === true) {
            favoritesRemove(movie)
        } else {
            favoritesAdd(movie)
        }
    }

    const getButtonColor = (movie) => {
        if (favoriteExists(movie) === false) {
            return Platform.OS === 'android' ? '#2196F3' : '#007AFF'
        }
        return Platform.OS === 'android' ? 'crimson' : 'crimson'
    }

    return { favorites, favoritesAdd, favoritesRemove, favoriteExists, clear, getById, getFavoritesActionText,manageFavorite, getButtonColor }
}

export default useFavorites
