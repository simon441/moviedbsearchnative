import React, { createContext, useState } from 'react'

const inititialState = {
    favorites: []
}
// const [favorites, setFavorites] = useState(undefined)
const FavoritesContext = createContext()
// const FavoritesContext = createContext({...inititialState}, () => {})

const FavoritesContextProvider = props => {
    const [favorites, setFavorites] = useState([])

    return (
        <FavoritesContext.Provider value={[favorites, setFavorites, ]}>
            {props.children}
        </FavoritesContext.Provider>
    )
}

export {FavoritesContext, FavoritesContextProvider}