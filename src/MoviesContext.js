import { useContext } from "react";
import React, { createContext, useState } from 'react'

const inititialState = {
    favorites: [],
    movies: [],
    results: []
}
// const [favorites, setFavorites] = useState(undefined)
const MoviesContext = createContext()
// const FavoritesContext = createContext({...inititialState}, () => {})

const MoviesContextProvider = props => {
    const [movies, setMovies] = useState({})

    return (
        <MoviesContext.Provider value={[movies, setMovies ]}>
            {props.children}
        </MoviesContext.Provider>
    )
}

export {MoviesContext, MoviesContextProvider}