import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation } from '@react-navigation/native'
import React, { useEffect } from 'react'
import { View, Text, StyleSheet, FlatList, TouchableOpacity, ScrollView, Button, Alert } from 'react-native'
import useFavorites from '../src/useFavorites'
import { useSearch } from '../src/useSearch'

const SettingsScreen = () => {
    const search = useSearch()
    const favorites = useFavorites()
    const navigation = useNavigation()

    useEffect(() => {
        console.log('settings screen');
        console.log(search.search, search.getAll(), search.search.length);
        console.log(search.allStored(), search.allStoredValue());
        // const s = AsyncStorage.getItem('@searchKey').then(value => console.log('sored value', value))
    }, [search])



    const onSearchPressed = (term) => {
        const found = search.find(term)
        console.log('onSearchPressed', term, found);
        if (found !== undefined) {
            navigation.navigate('Home', { search: found })
        }
    }

    const handleClearFavorites = () => {
        handleClearAction('favorites', () => favorites.clear())
    }

    const handleClearHistory = () => {
        handleClearAction('search history', () => search.clear())
    }

    const handleClearAllStoredData = () => {
        handleClearAction('favorites and search history', () => {
            search.clear()
            favorites.clear()
        })
    }

    const handleClearAction = (actionName, callback) => {

        Alert.alert('Movie DB', `Delete all ${actionName}?`, [
            {
                text: 'OK',
                onPress: callback,
                style: 'destructive'
            },
            {
                text: 'Cancel',
                onPress: ()=> {},
                style: 'cancel'
            }
        ],
        {cancelable: true})

    }

    return (
        <ScrollView style={styles.container}>
            <Text style={styles.font}>Settings</Text>
            <View style={styles.section}>
                <Text style={[styles.searchFont, { fontSize: 20, marginTop: 20, marginBottom: 10 }]}>Search History</Text>
                <Text style={styles.searchFont}>Tap to search again</Text>
                {/* <Text style={styles.searchFont}>{search.search.length}</Text> */}
                {/* <FlatList
                    data={search.search}
                    renderItem={({ item, index }) => (
                        <TouchableOpacity key={index} onPress={() => onSearchPressed(item)}>
                            <Text style={styles.searchFont}>{item}</Text>
                        </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => item}
                /> */}

                    <ScrollView style={styles.searchFont}>
                        {search.search.map((term, index) => {
                            return (
                                <TouchableOpacity key={term} onPress={() => onSearchPressed(term)}>
                                    <View key={index} style={{ flex: 1, flexDirection: 'row', flexGrow: 1 }}>
                                        <Text style={[styles.searchTerms, {flexGrow:1}]}>{term}</Text>
                                        <Text onPress={() => search.remove(term)} style={[styles.searchTerms,{ color: '#fff', backgroundColor: 'red',flexGrow: 0 }]}>Delete</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </ScrollView>
                {search.getAll().length > 0 && (
                    <View><Button title='Clear search history' onPress={() => handleClearHistory()} /></View>
                )}
            </View>
            <View style={styles.section}>
                <Text style={[styles.searchFont, { fontSize: 20, marginTop: 20, marginBottom: 10 }]}>Favorites</Text>
                <Button title="Clear Favorites" onPress={() => handleClearFavorites()}/>
            </View>
            <View style={styles.section}>
            <Text style={[styles.searchFont, { fontSize: 20, marginTop: 20, marginBottom: 10 }]}>Others</Text>
                <Button title="Clear All Stored Data" onPress={() => handleClearAllStoredData()}/>
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    footer: {
        color: '#fff',
        textAlign: 'center',
        fontSize: Platform.OS === 'web' ? 15 : 10,
        paddingBottom: Platform.OS === 'web' ? 40 : 1,
        fontSize: 16,
        marginHorizontal: 5
    },
    container: {
        backgroundColor: '#223343',
        flex: 1,
        fontSize: 24
        // justifyContent: 'flex-end'
    },
    font: {
        fontSize: 24,
        textAlign: 'center',
        color: '#fff'
    },
    searchFont: {
        color: '#fff'
    },
    white: {
        color: 'whitesmoke'
    },
    heading: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '700',
        padding: 20,
        backgroundColor: '#445565'
    },
    searchTerms: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '400',
        padding: 5,
        backgroundColor: '#445565',
        marginVertical:5,
        borderBottomColor:'#aaa',
        // borderBottomWidth:5
    },
    section: {flex: 0,
        marginBottom: 20,
        marginTop: 0
    }
})

export default SettingsScreen
