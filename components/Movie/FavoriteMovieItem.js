import React, { useEffect } from 'react'
import { View, Text, Button, StyleSheet, TouchableHighlight, Image } from 'react-native'

const FavoriteMovieItem = ({movie, openPopup}) => {
    useEffect(() => {
        console.log('movieitem', movie);
    }, [])

    return (
        <View style={styles.container}>
            {/* <TouchableHighlight
                onPress={() => openPopup(movie.imdbID)}
                key={movie.imdbID}
            > */}
                <View style={styles.movie} key={movie.imdbID}>
                    <Text style={styles.heading}>{movie.Title}</Text>
                    {movie.poster !== "N/A" && <Image
                        source={{ uri: movie.Poster }}
                        style={{
                            width: '100%',
                            height: 300
                        }}
                        resizeMethod='scale'
                        resizeMode="contain"
                    />}
                </View>
            {/* </TouchableHighlight> */}
            <Button title='See Details' onPress={() => openPopup(movie.imdbID)} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginBottom:50
    },
    movie: {
        flex: 1,
        width: '100%',
    },
    heading: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '700',
        padding: 20,
        backgroundColor: '#445565'
    },
    buttonRemoveColor: {

    }
})

export default FavoriteMovieItem
