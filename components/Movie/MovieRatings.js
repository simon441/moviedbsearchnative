import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const MovieRatings = ({ ratings }) => {
    return (
        <View style={{marginLeft: 10}}>
            {ratings?.map((rating, index) => (
                <Text style={styles.details_text} key={index}>{rating.Source} {rating.Value}</Text>
            ))}
        </View>
    )
}

const styles = StyleSheet.create({
    details_text: {
        fontSize: 16
    },
})

export default MovieRatings
