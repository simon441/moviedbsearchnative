import React, { useEffect } from 'react'
import { View, Text, StyleSheet, TouchableHighlight, Image, Platform, ScrollView, Button } from 'react-native'
import { color } from 'react-native-reanimated'
import useWindowDimensions from 'react-native/Libraries/Utilities/useWindowDimensions'
import useFavorites from '../../src/useFavorites'
import { isNotNA } from '../../src/utils'
import FavoriteMovieItem from './FavoriteMovieItem'
import MovieRatings from './MovieRatings'

const Movie = ({ movie, handleCloseModal }) => {

    const dimensions = useWindowDimensions()

    const context = useFavorites()

    /**
     *
     * @param {string} minutes Minutes (format: <minutes>min)
     * @returns {string} (format <hour>H<minutes>min)
     */
    const convertMinToHourMin = (minutes = '') => {
        const time = parseInt(minutes.substring(0, minutes.length - 'min'.length).trim(), 10)
        if (typeof time === NaN && minutes === '') {
            return minutes
        }
        const computedHours = Math.floor(time / 60)
        const computedMinutes = time - (computedHours * 60)
        return `${computedHours}H ${computedMinutes}min`
    }

    // console.log(convertMinToHourMin(movie.Runtime));

    useEffect(() => console.log('movie', movie), [])
    // useEffect(() => {
    //     if (dimensions.width < 600) {
    //         styles.details_view.flexDirection = 'column'
    //     }
    //     return () => {

    //     }
    // }, [input])
    return (<ScrollView>
        <View style={styles.popup}>
            <Text style={styles.poptitle}>{movie.Title}</Text>
            <Text >{movie.Year} - {movie.Rated} - {convertMinToHourMin(movie.Runtime)}</Text>
            {isNotNA(movie.Poster) && <Image source={{uri: movie.Poster}} style={{
                minHeight: 427,
                minWidth: 300,
                width: '100%'
            }}
                resizeMode='contain'
                resizeMethod='scale' />
            }
            <Text style={{ marginVertical: Platform.OS === 'web' ? '1rem' : 16 }}>{movie.Plot}</Text>

            <View style={styles.details_view}><Text style={styles.details_type}>Genre</Text><Text style={[styles.genre, styles.details_text]}>{movie.Genre}</Text></View>
            <View style={[styles.details_view, { marginBottom: 20 }]}><Text style={[styles.details_type]}>Rating: </Text><Text style={styles.details_text}>{movie.imdbRating}</Text></View>
            {isNotNA(movie.Director) && <View style={styles.details_view}><Text style={styles.details_type}>Director:</Text><Text style={styles.details_text}> {movie.Director}</Text></View>}
            {isNotNA(movie.Writer) && <View style={styles.details_view}><Text style={styles.details_type}>Writer:</Text><Text style={styles.details_text}> {movie.Director}</Text></View>}
            {isNotNA(movie.Actors) && <View style={styles.details_view}><Text style={styles.details_type}>Actors:</Text><Text style={styles.details_text}> {movie.Actors}</Text></View>}

            {/* <Text style={styles.more}></Text>
            <Text style={{}}>Details</Text> */}
            <View style={styles.details_view}><Text style={styles.details_type}>Release date</Text><Text style={styles.details_text}>{movie.Released}</Text></View>
            <View style={styles.details_view}><Text style={styles.details_type}>Country of origin</Text><Text style={styles.details_text}>{movie.Country}</Text></View>
            <View style={styles.details_view}><Text style={styles.details_type}>Language</Text><Text style={styles.details_text}>{movie.Language}</Text></View>
            <View style={styles.details_view}><Text style={styles.details_type}>Awards</Text><Text style={styles.details_text}>{movie.Awards}</Text></View>
            <View style={styles.details_view}><Text style={styles.details_type}>Type</Text><Text style={styles.details_text}>{movie.Type}</Text></View>
            {movie.Type === 'series' && <View style={styles.details_view}><Text style={styles.details_type}>Seasons</Text><Text style={styles.details_text}>{movie.totalSeasons}</Text></View>}
            {movie.Type === 'movie' && (
                <>
                    <View style={styles.details_view}><Text style={styles.details_type}>Box Office</Text><Text style={styles.details_text}>{movie.BoxOffice}</Text></View>
                    <View style={styles.details_view}><Text style={styles.details_type}>DVD Release</Text><Text style={styles.details_text}>{movie.DVD}</Text></View>
                </>
            )}
            {movie.Production && <View style={styles.details_view}><Text style={styles.details_type}>Box Office</Text><Text style={styles.details_text}>{movie.BoxOffice}</Text></View>}

            <View style={styles.details_view}><Text style={styles.details_type}>IMDB Ratings</Text><Text style={styles.details_text}>{movie.imdbRating}/10 ({movie.imdbVotes})</Text></View>

            {movie.Ratings && <View style={[styles.details_view, { flexDirection: 'column' }]}><Text style={styles.details_type}>Ratings: </Text><MovieRatings ratings={movie.Ratings} /></View>}

            <View style={{ margin: 20 }}><Text> </Text></View>
            <Button title={context.getFavoritesActionText(movie)} color={context.getButtonColor(movie)} onPress={() => context.manageFavorite(movie)} />
            <View style={{ margin: 20 }}><Text> </Text></View>

            <TouchableHighlight
                onPress={() => handleCloseModal()}
            >

                <Text style={styles.closeBtn}>Close</Text>
            </TouchableHighlight>
        </View></ScrollView>
    )
}

const styles = StyleSheet.create({
    popup: {
        padding: 20,
        backgroundColor: '#fff'
    },
    poptitle: {
        fontSize: 24,
        fontWeight: '700',
        marginBottom: 5
    },
    rating: {

    },
    genre: {
        fontSize: Platform.OS === 'web' ? '.875rem': 24,
        fontWeight: '400',
        borderColor: '#ccc',
        borderRadius: 5
    },
    more: {
        borderBottomWidth: 5,
        borderBottomColor: '#ddd'
    },
    details_view: {
        // display: 'flex',
        flex: Platform.OS ==='web' ? 1 : 0,
        flexDirection: Platform.OS === 'web' ? 'row' : 'column',
        justifyContent: 'flex-start',
        paddingBottom: 10,
        fontSize: 24
    },
    details_type: {
        fontWeight: '700',
        fontSize: 16,
        marginRight: 5
    },
    details_text: {
        fontSize: 16
    },
    bold: {
        fontWeight: '900',
        fontSize: 16
    },
    closeBtn: {
        padding: 20,
        fontSize: 24,
        fontWeight: '700',
        backgroundColor: '#2484c4',
        color: '#fff'
    }
})

export default Movie
