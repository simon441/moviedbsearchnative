import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const AboutScreen = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.font}>About Movie DB Search</Text>
            <View style={{ flex: 0, marginBottom: 20, marginTop: 0 }}>
                <View style={{ marginTop: 30 }}><Text style={styles.footer}>Powered by the Open Movie Database API omdbapi.com</Text></View>
                <View><Text style={styles.footer}>Copyright &copy; 2021 Simon Cateau</Text></View></View>
        </View>
    )
}
const styles = StyleSheet.create({
    footer: {
        color: '#fff',
        textAlign: 'center',
        fontSize: Platform.OS === 'web' ? 15 : 10,
        paddingBottom: Platform.OS === 'web' ? 40 : 1,
        fontSize: 16,
        marginHorizontal: 5
    },
    container: {
        backgroundColor: '#223343',
        flex:1,
        fontSize: 24
        // justifyContent: 'flex-end'
    },
    font: {
        fontSize:24,
        textAlign: 'center',
        color: '#fff'
    }
})

export default AboutScreen
