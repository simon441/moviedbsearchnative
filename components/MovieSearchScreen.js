import React, { useContext, useEffect, useState } from 'react';
import { ActivityIndicator, Alert, Button, Modal, Platform, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TextInput, TouchableHighlight, View } from 'react-native';
import Movie from './Movie/Movie'
import MovieList from './MovieList';
import { config } from '../env'
import { useNetInfo } from '@react-native-community/netinfo';
import { FavoritesContext } from '../src/FavoritesContext';
import useFavorites from '../src/useFavorites';
import useMovies from '../src/useMovies';
import { useSearch } from '../src/useSearch';
import { useNavigation, useRoute } from '@react-navigation/native';

export default function MovieSearchScreen() {

  const API_URL = `http://www.omdbapi.com/?apikey=${config.API_KEY}`

  const DEFAULT_INPUT = 'Enter a Movie...'

  const ALLOWED_SEARCHED_TYPES = ['movie', 'series', 'episode']

  const [state, setState]= useState({
    s: DEFAULT_INPUT,
    type: 'movie',
    results: [],
    selected: {},
    localResults: false
  })

  const contextFavorites = useFavorites()

  const contextMovies = useMovies()

  const contextSearch = useSearch()

  const [isOpenModal, setIsOpenModal] = useState(false)

  // const {currentPage, next, previous, totalPages} = usePagination(1)

  const [previousSearch, setPreviousSearch] = useState([])

  // const [favorites, setFavorites] = useContext(FavoritesContext)

  const [action, setAction] = useState('+')

  const [searchResults, setSearchResults] = useState([])

  const [movies, setMovies] = useState([])

  const [hasSearched, setHasSearched] = useState(false)

  const [currentPage, setCurrentPage] = useState(1)

  const [isLoading, setIsLoading] = useState(false)

  const FETCH_TIMEOUT = 0


  const netInfo = useNetInfo()


  // const {next, prev, currentPage, jump, currentData, maxPage} = usePagination(state.results, 10)

  /**
   * Search for movies and store into state to prevent fetching the same search
   */
  const handleSearch = () => {
    setState(previousState => { return { ...previousState, localResults: false, selected: {} }})
    setCurrentPage(1)

    const search = state.s.trim()
    if (search === DEFAULT_INPUT || search === '') {
      Alert.alert('Movie DB', 'Please enter a movie name')
      return
    }
    contextSearch.add(search)
    fetchSearch(search)
  }

  const fetchSearch = (search, page = 1, fetchMore = false) => {
      // console.log(state, `${API_URL}&s=${state.s}`);
      if (search === DEFAULT_INPUT || search === '') {
        Alert.alert('Movie DB', 'Please enter a movie name')
        return
      }
    setIsLoading(true)
    console.log('fetchSearch');
    if (previousSearch.length > 0) {
      const searched = previousSearch.filter(s => s.search === search && s.page === page)

      console.log('searched', searched, search, searched.results);
      // search has already been made return results
      if (searched.length > 0) {
        const results = searched[0].results

        setState(previousState => {
          return { ...previousState, results, localResults: true }
          // return { ...previousState, ...{ results: fetchMore ? [...previousState.results, ...results] : results }, localResults: true }
        })

        setCurrentPage(page)
        setHasSearched(true)
        setIsLoading(false)
        return true
      }
    }
    if (netInfo.isConnected === false) {
      Alert.alert('Movie DB - Error', 'Cannot retrieve movies.\nNo network connection')
      setIsLoading(false)
      setHasSearched(false)
      return
    }
    setTimeout(() => {
      // fetch search
      fetch(`${API_URL}&s=${search}&page=${page}`)
        .then(response => response.json())
        .then(data => {
          const results = data.Search.filter(val => ALLOWED_SEARCHED_TYPES.includes(val.Type))
          if (data.Response === 'True') {
            setState(previousState => {
              // console.log(previousState.results, results, [previousState.results, ...results]);
              const r = previousState.results.length > 0 ? [...previousState.results, ...results] : results
              console.log('r', r);
              const newState = { results: [...previousState.results, ...results] }
              console.log('newState', newState, {
                ...previousState,
                ...newState
              });
              if (loadMore === true) {
                return {
                  ...previousState,
                  ...newState
                }
              }
              return {
                ...previousState,
                results
              }
            })
            setSearchResults(prev => {
              const newSearch =
                prev.map(s => (prev[search] === undefined) ? { [search]: results } : { [search]: prev[search].concat(...results) })

              return newSearch
            })
            setPreviousSearch(previousState => {
              const r = previousState.results?.length > 0 ? [...previousState.results, ...results] : results
              const f = previousState?.results?.filter(s => s.search === search)
              console.log('previousSearch', r, f)


              if (f === undefined || f.length === 0) {
                return [
                  ...previousState,
                  { search, results, page }
                ]
              } else {
                const res = { search, results: r }
                console.log('res', res)
                const newState = previousState.map(cur => (cur.search === search && cur.page === page) ? { search, results, page } : cur)
                console.log('newState', newState);
                return newState
                return [
                  ...previousState,
                  newState
                ]
              }
            }
            )
          }

          setCurrentPage(page)
        })
        .catch(err => console.error(err))
        .finally(() => {
          setHasSearched(true)
          setIsLoading(false)
        })
    }, FETCH_TIMEOUT);
  }

  /**
   * Open the Modal to display the selected Movie
   * @param {number} id - imdbID
   */
  const openPopup = (id) => {
    setIsOpenModal(true)
    if (movies.length > 0) {
      const selected = movies.filter(movie => movie.imdbID === id)
      setState(previousState => {
        return { ...previousState, selected }
      })
    }
    if (netInfo.isConnected === false) {
      Alert.alert('Movie DB - Error', 'Cannot retrieve movie.\nNo network connection')
      setIsLoading(false)
      setHasSearched(false)
      setIsOpenModal(false)
      return
    }
    fetch(`${API_URL}&i=${id}`)
      .then(response => response.json())
      .then(data => {
        const selected = data
        setState(previousState => {
          return { ...previousState, selected }
        })
        setMovies(prev => [...prev, selected])
        contextMovies.moviesAdd(selected)
        setIsOpenModal(true)
      })
  }

  /**
   * Close the Modal
   */
  const handleCloseModal = () => {
    setState(previousState => {
      return { ...previousState, selected: {} }
    })
    setIsOpenModal(false)
  }

  const fetchNextResults = () => {
    // setCurrentPage(prev => prev + 1)
    fetchSearch(state.s, currentPage + 1, true)
  }
  const fetchPreviousResults = () => {

    console.log('currentPage', currentPage);
    if (currentPage === 1) {
      return
    }
    // setCurrentPage(prev => prev - 1)
    fetchSearch(state.s, currentPage - 1, true)
  }

  const getCurrentPageResults = () => {
    return state.results
  }

  const loadMore = () => {
    // setCurrentPage(prev => prev + 1)
    fetchSearch(state.s, currentPage + 1, true)
  }

  const addToFavorites = (favorite) => {
    // setFavorites(fav => [...fav, favorite])
    contextFavorites.favoritesAdd(favorite)
    setAction('-')
  }
  const removeFromFavorites = (favorite) => {
    contextFavorites.favoritesRemove(favorite)
    // const newFavorites = favorites.filter(fav => fav.imdbID !== favorite.imdbID)
    setAction('+')
    // setFavorites(newFavorites)
  }

  const setFavoriteAction = (favorite) => {
    let type = '+'
    // if (favorites.find(fav => fav.imdbID === favorite.imdbID) !== undefined) {
    if (contextFavorites.favoriteExists(favorite.imdbID) === true) {
      type = '-'
    }
    setAction(type)
    return type
  }
  const getFavoriteAction = (favorite) => {
    let type = '+'
    // console.log(favorites.find(fav => fav.imdbID === favorite.imdbID));
    if (contextFavorites.favoriteExists(favorite.imdbID) === true) {
      type = '-'
    }
    return type
  }
  useEffect(() => {
    // console.log('favorites changed', contextFavorites.favorites, action);
    // setAction(ac => ac + 'a')
  }, [contextFavorites.favorites])

  // const manageFavorite = (favorite) => {

  //   if (action === '-') {
  //     removeFromFavorites(favorite)
  //   }
  //   if (action === '+') {
  //     addToFavorites(favorite)
  //   }
  // }
  const navigation = useNavigation()
  const route = useRoute()

  useEffect(() => {
    console.log('ok');
    setIsOpenModal(false)
  }, [])

  useEffect(() => {
    console.log('routeeeeeeeeeeeeeee', route);
    if (route?.params !== undefined) {
      const term = route.params.search
      const searchList = contextSearch.getAll()
      console.log(searchList);
      if (searchList.length > 0) {
        const found = searchList.find(s => s === term)
        console.log('found', found, found !== undefined);
        if (found !== undefined) {
          setState(previousState => {
            return { ...previousState, s: found }
          })
         console.log(state.s);
         setState(previousState => { return { ...previousState, localResults: false, selected: {} }})
         setCurrentPage(1)
         fetchSearch(found)
        }
      }
    }
  }, [route])

  const generateNextPrevButton = () => (
    <View style={{ flexDirection: 'row', flex: 0, justifyContent: 'space-between', borderColor: '#fff', backgroundColor: '#ccc' }}>
      <View style={{ flexGrow: 1, marginRight: 5, width: '50%' }}>
        <Button title='Previous' onPress={() => fetchPreviousResults()} />
      </View>
      <View style={{ flexGrow: 1, marginLeft: 5, width: '50%' }}><Button title='Next' onPress={() => fetchNextResults()} />
      </View>
    </View>
  )

  const generateLoadMoreButton = () => (
    <View>
      <Button title='Load More' onPress={() => fetchNextResults()} />
    </View>
  )

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Text style={styles.title}>Movie DB</Text>
        <TextInput
          style={styles.searchbox}
          onChangeText={(text) => setState(previousState => {
            return { ...previousState, s: text }
          })}
          selectTextOnFocus={true}
          onSubmitEditing={handleSearch}
          value={state.s}
          autoCapitalize='words'
        />

        {isLoading ?
          (<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <ActivityIndicator animating={true} accessibilityLabel='Loading...' size='large' color='#00ff00' />
            {/*<Loader />*/}
            {/* <Text style={{ fontWeight: '700', color: '#ddd', textAlign: 'center', marginTop: 10 }}>Loading...</Text> */}
          </View>
          )
          : (
            state.results?.length > 0
              ?
              (<>
                <MovieList movies={getCurrentPageResults()} openPopup={openPopup} addToFavorites={addToFavorites} removeFromFavorites={removeFromFavorites} action={action} getFavoriteAction={getFavoriteAction} action={action} />
                {/* <TouchableHighlight
                onPress={() => fetchNextResults()}
              >
                <Text style={{ color: '#fff', backgroundColor: '#fa4c50', fontSize: 24 }}>More</Text>
              </TouchableHighlight> */}
                {generateNextPrevButton()}
                {/* {generateLoadMoreButton()} */}
              </>)
              : (
                hasSearched &&
                <Text style={{ color: '#fff', fontSize: 18, textAlign: 'center' }}>No result for "{state.s}"</Text>
              )
          )}
        {
          //  (
          // (Platform.OS !== 'web'  && typeof state.selected.Title === undefined) || (Platform.OS === 'web' && typeof state.selected.Title !== undefined)) &&
          <Modal
            animationType='fade'
            transparent={false}
            visible={isOpenModal === true}
            onDismiss={() => handleCloseModal()}
            onRequestClose={() => handleCloseModal()}
          >
            <Movie movie={state.selected} handleCloseModal={handleCloseModal} />
          </Modal>}
        {/* <View style={{flex:0, marginBottom: 20, marginTop: 0}}>
          <View style={{ marginTop: 30 }}><Text style={styles.footer}>Powered by the Open Movie Database API omdbapi.com</Text></View>
          <View><Text style={styles.footer}>Copyright 2021 Simon Cateau</Text></View></View> */}

        <StatusBar barStyle="light-content" showHideTransition='fade' animated={true} networkActivityIndicatorVisible={true} translucent={true} />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#223343',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: Platform.OS === 'web' ? 70 : 10,
    paddingHorizontal: 20
  },
  title: {
    color: '#fff',
    fontSize: 32,
    fontWeight: '700',
    textAlign: 'center',
    marginBottom: 20
  },
  searchbox: {
    fontSize: 20,
    fontWeight: '300',
    padding: 20,
    width: Platform.OS === 'web' ? '100%' : 'auto',
    backgroundColor: '#fff',
    borderRadius: 8,
    marginBottom: 40
  },
  popup: {
    padding: 20
  },
  footer: {
    color: '#fff',
    textAlign: 'center',
    fontSize: Platform.OS === 'web' ? 15 : 10,
    paddingBottom: Platform.OS === 'web' ? 40 : 1
  }
});
