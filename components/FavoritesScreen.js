import { useNetInfo } from '@react-native-community/netinfo'
import React, { useEffect, useState } from 'react'
import { View, Text, FlatList, Alert, Modal, ScrollView, ActivityIndicator, StyleSheet } from 'react-native'
import useFavorites from '../src/useFavorites'
import useMovies from '../src/useMovies'
import { config } from './config'
import FavoriteMovieItem from './Movie/FavoriteMovieItem'
import Movie from './Movie/Movie'

const FavoritesScreen = () => {
    const favorites = useFavorites()
    const movies = useMovies()

    const netInfo = useNetInfo()

    const [isOpenModal, setIsOpenModal] = useState(false)
    const [selected, setSelected] = useState(null)

    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState(true)

    useEffect(() => {
        console.log('favorites', favorites.favorites);
    }, [favorites])

    /**
   * Open the Modal to display the selected Movie
   * @param {number} favId - imdbID
   */
    const openPopup = (favId) => {
        setIsOpenModal(true)
        setError(true)
        setIsLoading(true)
        const filteredFavorites = favorites.getById(favId)
        console.log('filteredFavorites', filteredFavorites);
        if (filteredFavorites.length === 1) {
            const movie = movies.getById(filteredFavorites[0])
            if (movie.length === 1) {
                setSelected(movie)
                setError(false)
                return
            }
        }
        setError(false)
        if (netInfo.isConnected === false) {
            Alert.alert('Movie DB - Error', 'Cannot retrieve movie.\nNo network connection')
            setIsLoading(false)
            setIsOpenModal(false)
            return
        }
        fetch(`${config.API_URL}&i=${favId}`)
            .then(response => response.json())
            .then(data => {
                movies.moviesAdd(data)
                setIsOpenModal(true)
                setSelected(data)
                setError(false)
            })
            .catch(err => {
                console.error(err);
                setError(true)
                Alert.alert('Movie Db - Error', 'Could not find the movie in the favorites')
                setIsOpenModal(false)
            })
            .finally(() => { })
        if (error) {
            // Alert.alert('Movie Db - Error', 'Could not find the movie in the favorites')
            setIsOpenModal(false)
        }
        setIsLoading(false)
    }

    /**
     * Close the Modal
     */
    const handleCloseModal = () => {
        setSelected(null)
        setIsOpenModal(false)
    }
    return (
        <View style={styles.container}>
            <Text>Favorites</Text>
            {favorites.favorites.length > 0 && (
                <>
                    <Text>{favorites.favorites.length}</Text>
                    <ScrollView>
                        {favorites.favorites.map((item, index) => (

                            <FavoriteMovieItem key={index} movie={item} openPopup={openPopup} />
                        ))}
                    </ScrollView>

                </>
            )}
            {isLoading ? (
                <ActivityIndicator animating={true} size='large' />
            ) : (
                <Modal
                    animationType='fade'
                    transparent={false}
                    visible={isOpenModal === true}
                    onDismiss={() => handleCloseModal()}
                    onRequestClose={() => handleCloseModal()}
                >
                    {selected && <Movie movie={selected} handleCloseModal={handleCloseModal} />}
                </Modal>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#223343',
        flex: 1
    }
})

export default FavoritesScreen
