import React from 'react'
import { View, Text, ScrollView, Image, StyleSheet } from 'react-native'
import useFavorites from '../src/useFavorites'
import MovieListItem from './MovieListItem'

/**
 *
 * @param {object[]} param0
 */
export default function MovieList({ movies, openPopup, addToFavorites, removeFromFavorites, getFavoriteAction, action }) {

    return (
        <ScrollView style={styles.movies}>
            {movies.map((movie, index) => {
                const action = getFavoriteAction(movie)
                return (
                <MovieListItem movie={movie}
                openPopup={openPopup}
                addToFavorites={addToFavorites}
                removeFromFavorites={removeFromFavorites}
                action={action}
                key={`${movie.imdbID}${movie.Title}`}
                 />
            )})}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    movies: {
        flex: 1,
        marginBottom:20,
        paddingBottom:5,
        borderBottomColor:'#fff',
        borderBottomWidth:5
    },
})
