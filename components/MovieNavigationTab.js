import React, { useEffect, useLayoutEffect, useState } from 'react'
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs"
import { NavigationContainer, useNavigation } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import MovieSearchScreen from './MovieSearchScreen'
import AboutScreen from './AboutScreen'
import { StyleSheet } from 'react-native'
import FavoritesScreen from './FavoritesScreen'
import { useContext } from 'react'
import { FavoritesContext } from '../src/FavoritesContext'
import SettingsScreen from './SettingsScreen'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const Tab = createMaterialTopTabNavigator()


export default function MovieNavigationTab() {
   const [favorites, setFavorites] = useState([])

   const navigation = useNavigation()

   useLayoutEffect(() => {
  //   navigation.setOptions({
  //     headerRight: (<FontAwesome5.Button name={'cogs'} accessibilityLabel='Settings' color='#fff'/>),})
  //
   },[])

  return (
      // <Tab.Navigator initialRouteName="Home">
      //   <Tab.Screen name="Home" component={MovieSearchScreen} options={{title: 'Movie DB Search Home'}} />
      //   <Tab.Screen name="About" component={AboutScreen} />
      // </Tab.Navigator>
      <Tab.Navigator initialRouteName="Home" tabBarOptions={{
        activeTintColor: '#2d4ca6',
        inactiveTintColor: 'gray',
        // style: styles.labelStyle,
        labelStyle: {fontSize:14},
      }}
      //  tabBarPosition='bottom'
      screenOptions={
        {
          // title: 'Movie DB',
        }
      }
      >
      <Tab.Screen name="Home" component={MovieSearchScreen} options={{title: 'Home'}}
      />
            <Tab.Screen name="Favorites" component={FavoritesScreen} />
      <Tab.Screen name="Settings" component={SettingsScreen} />
      <Tab.Screen name="About" component={AboutScreen} />
    </Tab.Navigator>
  )
}


const styles = StyleSheet.create({
  labelStyle: {
    fontSize: 16
  }
})