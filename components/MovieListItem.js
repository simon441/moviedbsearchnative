import React, { useContext } from 'react'
import { View, Text, Image, StyleSheet, TouchableHighlight, Button, Platform } from 'react-native'
import { FavoritesContext } from '../src/FavoritesContext'
import useFavorites from '../src/useFavorites'
import { isNotNA } from '../src/utils'

const MovieListItem = ({ movie, openPopup, addToFavorites, removeFromFavorites, action }) => {

    const context = useFavorites()

    const getFavoritesActionText = () => {
        if (context.favoriteExists(movie) === true) {
            return 'Remove From Favorites'
        }
        return 'Add To Favorites'
        if (action === '+') {
            return 'Add To Favorites'
        } else {
            return 'Remove From Favorites'
        }
    }

    const doAction = () => {
        if (context.favoriteExists(movie) === true) {
            context.favoritesRemove(movie)
        } else {
            context.favoritesAdd(movie)
        }
        return

        if (action === '-') {
            removeFromFavorites(movie)
        }
        if (action === '+') {
            addToFavorites(movie)
        }
    }

    const getColor = () => {
        // if (action == '+') {
        if (context.favoriteExists(movie) === false) {
            return Platform.OS === 'android' ? '#2196F3' : '#007AFF'
        }
        // if (action == '-') {
        return Platform.OS === 'android' ? 'crimson' : 'crimson'
        // }
    }

    return (
        <View>
            <TouchableHighlight
                onPress={() => openPopup(movie.imdbID)}
                key={movie.imdbID}
            >
                <View style={styles.movie} key={movie.imdbID}>
                    <Text style={styles.heading}>{movie.Title}</Text>
                    {movie.poster !== "N/A" && <Image
                        source={{ uri: movie.Poster }}
                        style={{
                            width: '100%',
                            height: 300
                        }}
                        resizeMethod='scale'
                        resizeMode="cover"
                    />}
                </View>
            </TouchableHighlight>
            <Button title={context.getFavoritesActionText(movie)} onPress={doAction} color={context.getButtonColor(movie)} />
        </View>
    )
}

const styles = StyleSheet.create({
    movie: {
        flex: 1,
        width: '100%',
        marginBottom: 20
    },
    heading: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '700',
        padding: 20,
        backgroundColor: '#445565'
    },
    buttonRemoveColor: {

    }
})

export default MovieListItem
